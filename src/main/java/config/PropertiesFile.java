package config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import components.*;

public class PropertiesFile {

	static Properties prop = new Properties();
	static String configPath = "C:\\Users\\fermi\\eclipse-workspace\\SeleniumFramework\\src\\test\\java\\config\\config.properties";

	public static void main(String[] args) {
		readPropertiesFile();
	}

	public static void readPropertiesFile() {
		try {
			InputStream input = new FileInputStream(configPath);

			prop.load(input);
			Initializer.browser = prop.getProperty("browser");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeProperties() {
		try {
			OutputStream output = new FileOutputStream(configPath);

			prop.setProperty("browser", "Firefox");
			prop.store(output, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
