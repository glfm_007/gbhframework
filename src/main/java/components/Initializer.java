package components;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import config.*;

public class Initializer {

	public static String browser;
	static WebDriver driver;

	private static Initializer instance = null;

	public static Initializer getInstance() {
		if(instance == null) {
			instance = new Initializer();
		}
		return instance;
	}

	public void initializeEnviroment() throws MalformedURLException
	{
		PropertiesFile.readPropertiesFile();

		String projectPath = System.getProperty("user.dir");

		if(browser.contains("Chrome")) {

			System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");

			driver = new ChromeDriver();

		}

		try {
			runTest();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void runTest() throws InterruptedException {
		driver.get("https://gbhqatest.firebaseapp.com/");
	}
}
