import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserTest {

	public static String browser;
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		setBrowser();
		setBrowserConfig();
		runTest();
	}

	public static void setBrowser() {
		browser = "Chrome"; 
	}

	public static void setBrowserConfig() {

		String projectPath = System.getProperty("user.dir");

		if(browser.contains("Chrome")) {

			System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");

			driver = new ChromeDriver();

		}
	}

	public static void runTest() throws InterruptedException {

		driver.get("https://gbhqatest.firebaseapp.com/");

		Thread.sleep(5000);

		driver.close();
	}

}
